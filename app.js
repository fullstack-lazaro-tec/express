const express = require('express');
const routes = require('./routes/routes');
const app = express();
const port = 3002;

// Usar Node,js body parsing middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true}));

routes(app);

// Iniciar servidor
const server = app.listen(port, function(error){
    if (error) 
        return console.log(`Error: ${error}`);
    
   console.log(`El servidor escucha en el puerto ${server.address().port}`);
});